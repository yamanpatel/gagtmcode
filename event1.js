 //One of the event from Paychex for conversion
 if (current_url.indexOf("demos/fsa-calculator") > -1) {
        jQuery('input').blur(function() {
            try {
                input_heading = jQuery(this).prev('h3').text();
                t_calcname = jQuery('#calc-hold').find('h1').text();
                result = jQuery('.result').find('span').text();
                num_of_emp = jQuery('#input-EE-count').val();
                per_emp = jQuery('#input-EE-contrib').val();
                if (!t_empty(input_heading) && !t_empty(t_calcname) && !t_empty(result) && !t_empty(num_of_emp) && !t_empty(per_emp)) {
                    input_heading = t_editval(input_heading);
                    t_calcname = t_editval(t_calcname);
                    ga('send', 'event', 'calc|' + t_calcname, input_heading, num_of_emp + "|" + per_emp + "|" + result, {'nonInteraction': 1});
                } else {
                    ga('t_logger.send', 'event', 'calc_t_' + t_domain_name, t_calcname + "|" + num_of_emp + "|" + per_emp + "|" + result, {'nonInteraction': 1});
                }
            } catch (e) {
                ga('t_logger.send', 'event', 'calc_t_catched_' + t_domain_name, t_calcname + e.message, {'nonInteraction': 1});
            }
        });
    }


//Conversion into pure javascript

//fetching the previous element 
if(typeof Element.prototype.TVCprevSiblings !== 'function'){
    Element.prototype.TVCprevSiblings = function(filter) {
        var sibs = [];
        var ele = this;
        while (ele = ele.previousSibling) {
            if (ele && ele.nodeType === 1) {
                if(filter){
                    if (ele.matches(filter)) {
                      sibs.push(ele);
                    }
                } else{
                    sibs.push(ele);
                }
            }
        }
        return sibs.length?sibs:null;
    };
}

//find 'input' and bind blur
var tvc_input_ele = document.getElementsByTagName('input');
for(i=0;i<tvc_input_ele.length;i++){
    tvc_input_ele[i].addEventListener("blur", myFunction); 
}

//fetch all the possible values for tracking
function myFunction() {
    try{
        input_heading = document.getElementsByTagName(this).TVCprevSiblings('h3')[0].innerText;
        result = document.querySelector('.result span').innerHTML;
        num_of_emp = document.querySelector('#input-EE-count').value;
        per_emp = document.querySelector('#input-EE-contrib').value;
        if (!t_empty(input_heading) && !t_empty(t_calcname) && !t_empty(result) && !t_empty(num_of_emp) && !t_empty(per_emp)) {
            input_heading = t_editval(input_heading);
            t_calcname = t_editval(t_calcname);
            //dataLayer with all the info
            dataLayer.push({'event':'tvc_migration_'+'calc_t_' + t_domain_name,
                'tvc_eventcat':input_heading,
                'tvc_eventact':t_domain_name,
                'tvc_eventlab':num_of_emp + "|" + per_emp + "|" + result});
        } else {
            //dataLayer with missing info
            dataLayer.push({'event':'tvc_missinginfo_'+'calc_t_' + t_domain_name,
                'tvc_eventcat':input_heading,
                'tvc_eventact':t_domain_name,
                'tvc_eventlab':num_of_emp + "|" + per_emp + "|" + result});   
        }
    }catch(e){
        //dataLAyer if execution fails
        dataLayer.push({'event':'tvc_errorcatch'+'calc_t_' + t_domain_name,
                'tvc_erromessage':e.message});
    }
}