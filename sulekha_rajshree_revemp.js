try{
var tvc_quick_link = document.querySelectorAll('body > div.quick-links > ul > li > a');
var tvc_hamburger_sub_menu = document.querySelectorAll('#slide-out > div.ss-wrapper > div > ul > li > ul > li > div > ul > li > a');
var tvc_hamburger_start = document.querySelector('div.button-collapse.nav-icon');
var tvc_hamburger_menu = document.querySelectorAll('#slide-out > div.ss-wrapper > div > ul > li > ul > li > a');
var tvc_header_nav_menu = document.querySelectorAll('ul.primary-nav > li > ul > li > a');
var tvc_hamburger_other = document.querySelectorAll('div.ss-content > ul > li > a');
var tvc_header_nav_main =  document.querySelectorAll('ul.primary-nav > li > a');
  
  if(tvc_hamburger_other !== null && tvc_hamburger_other.length > 0){
    for(i=0;i<tvc_hamburger_other.length;i++){
      tvc_hamburger_other[i].addEventListener('click', function(e){
        var parent_element = this.innerText;
        tvc_hamburgerTrack(parent_element, '');
      });
    }
  }
  
  if(tvc_quick_link !== null && tvc_quick_link.length > 0){
    for(i=0;i<tvc_quick_link.length;i++){
      tvc_quick_link[i].addEventListener('click', function(e){
        var link_text = this.innerHTML;
        dataLayer.push({event : 'quick_link', 'action' : 'quick link', 
					'label' : link_text});
      });
    }
  }

if(tvc_hamburger_sub_menu !== null && tvc_hamburger_sub_menu.length > 0){
  for(i=0;i<tvc_hamburger_sub_menu.length;i++){
    tvc_hamburger_sub_menu[i].addEventListener('click', function(e){
      var click_element = this.innerText;
      var parent_element = this.parentElement.parentElement.parentElement.parentElement.children[0].innerText;
      tvc_hamburgerTrack(parent_element ,click_element);
    });
  }
}

if(tvc_hamburger_start !== null && tvc_hamburger_start.length > 0){
tvc_hamburger_start.addEventListener('click',function(e){  
  if(this.className.indexOf('open') != -1)
  	  {tvc_hamburgerTrack('open', ''); }
  else{tvc_hamburgerTrack('close', ''); }
			
  });
}

if(tvc_hamburger_menu !== null && tvc_hamburger_menu.length > 0){
  for(i=0;i<tvc_hamburger_menu.length;i++){
    tvc_hamburger_menu[i].addEventListener('click', function(e){
      if(this.classList.contains('active')){
        var parent_element = this.innerText;
        tvc_hamburgerTrack(parent_element, '');
      }
    });
  }
}

if(tvc_header_nav_main !== null && tvc_header_nav_main.length > 0){
  for(i=0;i<tvc_header_nav_main.length;i++){
    tvc_header_nav_main[i].addEventListener('click', function(e){
      var click_element = this.innerHTML;
      click_element = click_element.toUpperCase();
      dataLayer.push({event : 'header_nav_menu', 
					'action' : click_element, 'label' : '' });	
    });
  }
}
if(tvc_header_nav_menu !== null && tvc_header_nav_menu.length > 0){
  for(i=0;i<tvc_header_nav_menu.length;i++){
    tvc_header_nav_menu[i].addEventListener('click', function(e){
   	var click_element = this.innerHTML;
    var parent_element = this.parentElement.parentElement.parentElement.children[0].innerText;
    dataLayer.push({event : 'header_nav_menu', 
					'action' : parent_element, 'label' : click_element });
    });
  }
}

function tvc_hamburgerTrack(parent_element, click_element){
	dataLayer.push({event : 'hamburger_menu', 
					'action' : parent_element, 'label' : click_element });
}
  
}catch(err){
  var tvc_errorlog = new Image(); 
  tvc_errorlog.src="https://script.google.com/macros/s/AKfycbyjSrREKQrMeL5QwouwHetE23RltRE976x11Oy3UrjKG0feoNDc/exec?tvc_error="+err.name+"&msg="+err.message+"&tagname=tvc_nav_menu_inject&sheetname=sulekha";
}
