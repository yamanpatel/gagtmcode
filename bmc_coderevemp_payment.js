//New Code by Yaman
//check hasClass like function
function tvc_hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}
function tvc_ie_trim(value) {
    value = value.replace(/^\s+|\s+$/g, '');
    return value;
}
//gift voucher tracking
if(document.getElementById('lnkGV') !== null){
  document.getElementById('lnkGV').addEventListener('click', function() {
    dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:'Gift Voucher',tvc_subcategory:''})
  });
}

//New Quik Pay
var tvc_quikpay = document.getElementById('btnPayLastQuikPay');
if(tvc_quikpay !== null){ 
  tvc_quikpay.addEventListener('click', function() {
    dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_quikpay.innerHTML,tvc_subcategory:''})
  });
}

//Payment options except gift voucher
var tvc_makepayment_button = document.querySelectorAll('button.btn._cuatro:not(#btnWalletSignIn)');
if(tvc_makepayment_button !== null){
  for(i=0;i<tvc_makepayment_button.length;i++){
    if(tvc_makepayment_button[i].innerHTML == 'MAKE PAYMENT'){
      tvc_makepayment_button[i].addEventListener('click', function() {
        var tvc_main_category = document.querySelector('ul#dPayTabs li._active').getAttribute('data-tabvalue');
        switch(tvc_main_category) {
          case "QuikPay":
          var tvc_quikpay_bank = 'CREDIT/ DEBIT CARDS';
          var temp_netbank_quikpay = document.getElementById('dNBList').children;
          for(i=0;i<temp_netbank_quikpay.length;i++){
            if(tvc_hasClass(temp_netbank_quikpay[i],'_active')){
              var tvc_quikpay_bank = 'NET BANKING | '+temp_netbank_quikpay[i].getAttribute('title');
            }
          }
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category, tvc_subcategory:tvc_quikpay_bank });
          break;
        
          case "Credit/ Debit Card":
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category,tvc_subcategory:''});
          break;

          case "Freecharge":
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category,tvc_subcategory:''});
          break;

          case "Net Banking":
          var temp_var = document.querySelector('div#dDNetBnk select#dNetBnksDrop').value;
          var tvc_bank_selected = document.querySelector('div#dDNetBnk select#dNetBnksDrop option[value="'+temp_var+'"]').innerText;
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category, tvc_subcategory:tvc_bank_selected});
          break;
          case "American Express ezeClick":
            dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category,tvc_subcategory:''});
          break;
          case "Other Wallets & Cash Card":
          var temp_walletname = document.querySelector('#dDOtherWallets button').parentNode.children[1];
          for(i=0;i<temp_walletname.children.length;i++){
            if(temp_walletname.children[i].style.display !== "none"){
              var tvc_walletname = tvc_ie_trim(temp_walletname.children[i].children[0].innerText);
            }
          }
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category, tvc_subcategory:tvc_walletname});    
          break;
          case "Redeem Points":
          var temp_redeem_list = document.getElementById('dRdmPntsList').children;
          for(i=0;i<temp_redeem_list.length;i++){
            if(tvc_hasClass(temp_redeem_list[i],'_active')){
              var tvc_redeemtext = temp_redeem_list[i].getAttribute('data-name');
            }
          }
          dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category, tvc_subcategory:tvc_redeemtext});
          break;
          case "Credit Voucher":
            dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'payment options', tvc_button_label:tvc_main_category,tvc_subcategory:''});
          break;
          default:
          break;
        }
      });
    }
  }
}

//offer 2
if(document.getElementById('offApplyBtn_cc-dc-nb') !== null){
  document.getElementById('offApplyBtn_cc-dc-nb').addEventListener('click', function() {
  var temp_bmsoffer = document.getElementById('offApplyBtn_cc-dc-nb').parentNode.parentNode.parentNode.children[0].children;
    for(i=0;i<temp_bmsoffer.length;i++){
      if(temp_bmsoffer[i].style.display !== "none"){
        var tvc_offer_first = temp_bmsoffer[i].innerText;
      } 
    }
    dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'Avail Offers & Discounts', tvc_button_label:'Credit/ Debit/ Netbanking',tvc_subcategory:tvc_offer_first});
  });
}

//offer 1
if(document.getElementById('offApplyBtn_disc-codes') !== null){
  document.getElementById('offApplyBtn_disc-codes').addEventListener('click', function() {
    dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'Avail Offers & Discounts', tvc_button_label:'Apply for BookMyShow Offer/Discount',tvc_subcategory:''})
  });
}

//offer 3
if(document.getElementById('offApplyBtn_mob-oprts') !== null){
  document.getElementById('offApplyBtn_mob-oprts').addEventListener('click', function() {
  var temp_bmsoffer = document.getElementById('offApplyBtn_mob-oprts').parentNode.parentNode.parentNode.children[0].children;
    for(i=0;i<temp_bmsoffer.length;i++){
      if(temp_bmsoffer[i].style.display !== "none"){
        var tvc_offer_first = temp_bmsoffer[i].innerText;
      } 
    }
    dataLayer.push({event:'tvc_payment',tvc_sectionclicked:'Avail Offers & Discounts', tvc_button_label:'Mobile Operators',tvc_subcategory:tvc_offer_first});
  });
}




